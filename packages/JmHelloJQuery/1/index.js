import { UIComponent } from 'yt-engine'
import $ from 'jquery'

export default class JmHelloJQuery extends UIComponent { 
  render() { 
    const { count } = this 

    const target = $(`<button>Hello JQuery! ${count}</button>`) 

    target[0].addEventListener('click', () => { 
      this.changeData({ 
        count: parseInt(count) + 1 
      }) 
    }) 

    return target[0] 
  } 
} 

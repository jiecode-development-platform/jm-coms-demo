# 项目说明

[文档地址](https://yttech.yuque.com/ttwfhm/lx1lik/sfyn7n)

## 安装

  ### 开发环境

  [nodejs](http://nodejs.cn/)

  [vscode](https://code.visualstudio.com/)

  ### 克隆到项目本地

  ```shell
    git clone gitlab@192.168.0.156:appplat-four/plate3.0-com-demo.git
  ```

  ### 安装开发依赖

  ```shell
    npm i
  ```

  ### 开启调试模式

  ```shell
    npm run serve
  ```

  ### 代码存放格式

  组件统一存放在packages目录下，由组件名称和版本号目录组成

## 编译打包

  ### 打包配置 jm.config.js - 默认全部打包

  ```javascript
    module.exports = {
      /**
       * 要打包的组件 { name,version }
       */
      coms: [],
      /**
       * 相对于packages的子目录
       */
      subdir: '',
    }
  ```

  ### 执行打包命令

  ```shell
    npm run build
  ```

## 组件编写

  ### 创建组件

    1. 在packages目录下创建组件，目录名称既组件的唯一名称(最好加上命名空间前缀如: Jm${组件名称} )
    2. 在组件目录下创建版本号目录，每个版本号目录内的所有内容独立为一个版本
    3. 版本号目录下创建两个文件( **index.js** 和 **config.xml** )
    4. 编写组件参考packages下的demo
    5. 编写xml参考

  ### 组件上传

    执行打包命令后的dist目录下的.zip文件是需要上传的文件，打包出来的目录是源码目录，只需要上传.zip文件即可（在平台 **组件开发** 模块上传即可）。

## 升级cli版本到最新

  ```shell
    npm i git+https://gitee.com/jiecode-development-platform/jm-coms-cli.git -D
  ```
  